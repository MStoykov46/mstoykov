//output: Enter a tank size:
//Input: 50
//Output: Enter fuel efficiency (km per liter):
//Input: 10
//Output: Enter price per liter:
//Input: 2.45
//Output:We can travel 500km. // tank * efficiency
//for every 100km it will cost 24.5 lv. // Price * eff 


#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    float tank, eff, price;
    tank = eff = price = 0.;
    cout << "Enter a tank size:";
    cin >> tank;
    cout << "Enter fuel efficiency (km per liter):" <<endl;
    cin >> eff;
    cout << "Enter price per liter:" <<endl;
    cin >> price;
    
    cout << "We can travel " << tank*100/eff << "km" << endl;
    cout << "For every 100km it will cost " << price*eff << " lv";
 

return 0;
}
