//output: Enter a number in metres
//Input: 600 m
//Output: 
//600 m = 0.6 km
//600 m = 6000dm
//600 m = 60000 cm 


#include <iostream>
using namespace std;

int main()
{
 int m = 0; // double ako ne e cqlo
 cout << "Enter a number in meters: ";
 cin >> m;
 cout << m << " m = " << m / 1000.0 << " km" << endl;
 cout << m << " dm = " << m * 10 << " dm";

return 0;
}
