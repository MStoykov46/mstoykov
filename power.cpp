// Example program
// output: Enter a number:
// Input: 5
// Output: 5^2 = 25, 5^3 = 125, 5^4 = 625, 5^5 = 3125
// cmath -> pow
// iostream -> cout, cin
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    float x = 0.; // zadavame promenliva sus nachalna stoinost
    cout << "Enter a number: ";
    cin >> x;
    cout << x << "^2 = " << pow(x,2)<< endl << x <<" ^3 = " << pow(x,3);
    return 0;
}
